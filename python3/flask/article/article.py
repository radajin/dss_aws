from flask import *
import pickle
import datetime
import numpy as np
from flask_sqlalchemy import SQLAlchemy
from flask_mongoalchemy import MongoAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql://root:dss@52.78.5.7/world"
app.config["MONGOALCHEMY_DATABASE"] = 'article'
app.config["MONGOALCHEMY_CONNECTION_STRING"] = 'mongodb://52.78.5.7:27017/article'

mongo_db = MongoAlchemy(app)
db = SQLAlchemy(app)

models = {}

def init():
    with open("./models/classification.pkl", "rb") as f:
        models["classification"] = pickle.load(f)

class ArticlePDJ(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    sentence = db.Column(db.String(500), nullable=False)
    category = db.Column(db.String(20), nullable=False)
    rdate = db.Column(db.TIMESTAMP, nullable=False)
    
    def __init__(self, sentence, category):
        self.sentence = sentence
        self.category = category
    
    def __repr__(self):
        return "<Article {}, {}>".format(self.sentence, self.category)

class ArticleM(mongo_db.Document):
    sentence = mongo_db.StringField()
    category = mongo_db.StringField()
    rdate = mongo_db.DateTimeField()

db.create_all()

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/predic", methods=["POST","GET"])
def predic():
    result = {}
    result["category"] = [ "정치", "경제", "사회", "생활/문화", "세계", "IT/과학"]

    model = models["classification"]

    # URL Query - ?setence=문자열
    sentence = request.values.get("sentence")

    result["result"] = list(np.round_(model.predict_proba([sentence])[0]*100, 2))
    
    # 카테고리 구하기 
    max_idx = result["result"].index(max(result["result"]))
    category = result["category"][max_idx]
    
    # Mysql - 데이터 베이스에 저장
    article = ArticlePDJ(sentence, category)
    db.session.add(article)
    db.session.commit()
    
    # Mongodb - 데이터 베이스에 저장
    article = ArticleM(sentence=sentence, category=category, rdate=datetime.datetime.now())
    article.save()

    return jsonify(result)

@app.route("/mysql/<category>")
def mysql(category):
    results = ArticlePDJ.query.filter_by(category=category)
    datas = []
    for result in results:
        datas.append({
            "sentence": result.sentence,
            "category": result.category,
            "rdate": result.rdate,
        })
    
    return jsonify(datas)

@app.route("/mongodb/<category>")
def mongodb(category):
    results = ArticleM.query.filter(ArticleM.category == category)
    datas = []
    for result in results:
        datas.append({
            "sentence": result.sentence,
            "category": result.category,
            "rdate": result.rdate,
        })
    return jsonify(datas)

init()

app.run(debug=True)
