from flask import *
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello DSS"

# HTML
@app.route("/user")
@app.route("/user/<username>")
def user(username="FC"):
    print("username", username)
    return render_template('profile.html', name=username)

# JSON
@app.route("/people")
def people():
    people = { "alice": 25, "jin": 35 }
    return jsonify(people)

app.run(debug=True, port=5000)
