import requests
from bs4 import BeautifulSoup

def naver_keywords():
    result = []
    response = requests.get("https://www.naver.com/")
    dom = BeautifulSoup(response.content, "html.parser")
    keywords = dom.select(".ah_roll_area > .ah_l > .ah_item")
    for keyword in keywords[:5]:
        result.append(keyword.select_one('.ah_k').text)
    return str(result)
