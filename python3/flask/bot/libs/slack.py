import requests, json

def send_slack(msg, channel="#dss", username="bot" ):
    webhook_URL = "https://hooks.slack.com/services/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"
    payload = {
        "channel": channel,
        "username": username,
        "icon_emoji": ":test:",
        "text": msg,
    }

    response = requests.post(
        webhook_URL,
        data = json.dumps(payload),
    )
