import requests

def forecast(lat=37.5665, lng=126.9780, TOKEN="40f97ad0a7447c68b7b7d74c39f08276"):
    url = "https://api.darksky.net/forecast/{}/{},{}".format(TOKEN, lat, lng)
    response = requests.get(url)
    json_obj = response.json()
    return json_obj["currently"]["summary"]
