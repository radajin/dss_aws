from flask import *
from libs.slack import send_slack
from libs.forecast import forecast
from libs.naver import naver_keywords

app = Flask(__name__)

@app.route("/")
def index():
    return "running server..."

# slack outgoing webhook function
@app.route("/slack", methods=["POST"])
def slack():
    username = request.form.get("user_name")
    token = request.form.get("token")
    text = request.form.get("text")

    print(username, token, text)

    if "날씨" in text:
        weather = forecast()
        send_slack(weather)

    if "네이버" in text:
        keywords = naver_keywords()
        send_slack(keywords)

    return Response(), 200

app.run(debug=True)
