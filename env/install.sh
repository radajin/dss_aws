#!/bin/bash

# 파일 이동
mv ~/dss_aws/* ~/
rm -rf ~/dss_aws

# vim 인코딩 설정
echo "set encoding=utf-8" >> ~/.vimrc
echo "set nu" >> ~/.vimrc

# 쉘스크립트 권한 수정
chmod 775 ~/env/*.sh

# apt 업데이트
sudo apt-get update -y
sudo apt-get upgrade -y

# pyenv
source ~/env/pyenv.sh

# timezone
source ~/env/set_timezone.sh

# nginx
sudo apt-get install -y nginx

# mysql
sudo apt-get install -y mysql-server
sudo apt-get install -y libmysqlclient-dev

# mongodb
sudo apt-get install -y mongodb

# redis
sudo apt install -y redis-server

# celery
sudo apt install -y python-celery-common
