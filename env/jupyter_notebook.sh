#!/bin/bash

# 설정 파일 생성
jupyter notebook --generate-config

# 설정 파일 수정
# 주피터 노트북 실행시 브라우져 열지 않도록 설정
echo "c.NotebookApp.open_browser = False"  >> ~/.jupyter/jupyter_notebook_config.py
# private ip 입력
echo "c.NotebookApp.ip = ''" >> ~/.jupyter/jupyter_notebook_config.py
# 비밀번호 dss
echo "c.NotebookApp.password = 'sha1:6600c5733ef3:b683d6afba16b3403fdf9a75ac38b7d8e7f733bb'"  >> ~/.jupyter/jupyter_notebook_config.py
