#!/bin/bash

sudo timedatectl set-timezone Asia/Seoul
sudo unlink /etc/localtime
sudo ln -s /usr/share/zoneinfo/Asia/Seoul /etc/localtime
timedatectl
