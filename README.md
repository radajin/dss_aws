# dss_aws

데이터 사이언스 스쿨 AWS 환경설정

## 0. port 활성화
- AWS 콘솔의 보안그룹에서 아래의 포트를 활성화
    - 80, 8888, 3306, 27017, 6379

## 1. install git

```shell
git clone https://gitlab.com/radajin/dss_aws.git
```

## 2. install programs, python packages, setting files

```shell
# 중간 중간에 설정 화면이 나오면 그냥 엔터 입력
source ~/dss_aws/env/install.sh
```

```shell
# python package
source ~/env/python_packages.sh
# jupyter notebook setting
source ~/env/jupyter_notebook.sh
```


## 3. setting

#### 3.1 jupyter notebook

- 설정파일 수정
```shell
sudo vi /home/ubuntu/.jupyter/jupyter_notebook_config.py
```

- jupyter_notebook_config.py 파일에 가장 아래에 프라이빗 IP를 입력
```shell
# 본인의 프라이빗 IP 주소 입력
c.NotebookApp.ip = '172.31.26.225'
```

- 주피터 노트북 실행
```shell
jupyter notebook
```

- browser에 `http://<public_ip>:8888`로 접속시 jupyter notebook 출력 확인

#### 3.2. nginx and flask

- nginx 설정 파일 수정
```shell
sudo vi /etc/nginx/sites-available/default

# nginx 설정 파일에 아래의 코드의 80을 9999로 수정
    listen 80 default_server;
    listen [::]:80 default_server;

# nginx 설정 파일에 아래의 코드를 추가
server {
    listen 80;
    location / {
        proxy_pass http://localhost:5000;
    }
}
```
- 재시작으로 설정 적용
```shell
sudo systemctl restart nginx
```

- flask 실행
```shell
python ~/python3/flask/hello/hello.py
```

- browser에 `http://<public_ip>/`로 접속시 Hello DSS 출력 확인


#### 3.3 mysql

- 패스워드 설정
```shell
# 보안관련 설정 (n-(패스워드 입력)-y-n-n-y)순으로 입력
sudo mysql_secure_installation

# 패스워드 설정 (보안설정지 패스워드를 dss로 입력하였을 경우)
sudo mysql
mysql> ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'dss';
mysql> grant all privileges on *.* to 'root'@'%' identified by 'dss';
mysql> FLUSH PRIVILEGES;
mysql> exit
```

- 외부접속 설정
```shell
# mysql 설정파일 bind-address = 0.0.0.0 으로 수정
sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
bind-address = 0.0.0.0
```

- 설정 적용을 위해서 재시작
```shell
sudo systemctl restart mysql
```


- world 데이터 베이스 추가
```shell
mysql -u root -p
Enter password:<mysql 패스워드 입력>
mysql> create database world;
mysql> use world;
mysql> source ~/database/world.sql
mysql> ALTER DATABASE world CHARACTER SET = utf8;
mysql> quit
```

- mysql 설치에 문제가 있는경우 아래와 같이 삭제
```shell
sudo apt remove --purge mysql-server
sudo rm -rf /etc/mysql /var/lib/mysql
sudo apt autoremove
sudo apt autoclean
sudo apt purge mysql*
```

#### 3.4 mongodb

```shell
# mongodb 설정 파일 수정
sudo vi /etc/mongodb.conf

# bind_ip를 아래와 같이 변경하여 모든 IP에서 접속할수 있도록 수정
bind_ip = 0.0.0.0

# mongodb 재시작
sudo systemctl restart mongodb
```

#### 3.5 celery

- redis 설정 파일 수정
```shell
sudo vi /etc/redis/redis.conf
```

/etc/redis/redis.conf
```shell
# 외부에서 접속할수 있도록 수정
bind 0.0.0.0 ::1   
# systemctl에서 management를 할수 있도록 수정
supervised systemd
```

- 설정 적용 및 실행 확인
```shell
# 설정 적용
sudo systemctl restart redis.service
# 실행 확인
sudo systemctl status redis.service
```

- redis cli
```
$ redis-cli
> ping
```


## 4. Flask App

#### 4.1 bot

- https://darksky.net/dev 에서 TOKEN 확인
- `libs/forcast.py`의 TOKEN 값을 설정
- slack의 incoming, outgoing webhook을 설정
- `libs/slack.py`의 channel과 webhook_URL을 수정

#### 4.2 article

- `article.py` 파일의 아래 DB의 IP를 퍼블릭 IP로 변경
```shell
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql://root:dss@<퍼블릭 IP>/world"
app.config["MONGOALCHEMY_CONNECTION_STRING"] = 'mongodb://<퍼블릭 IP>:27017/article'
```
